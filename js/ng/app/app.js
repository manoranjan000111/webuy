"use strict";
var ngApp = angular.module("ngApp", [
    "ui.router",
    "oc.lazyLoad",
    'angular-loading-bar',
    'ngAnimate',
    "ngSanitize",
    "ngResource",
    "ngTable",
]);
