ngApp.run(["$rootScope", "$state", "auth", function ($rootScope, $state, auth) {
        $rootScope.$on('$stateChangeStart', function () {
            $rootScope.stateIsLoading = true;
        });
        $rootScope.$on('$stateChangeError', function () {
            $state.go('404', {}, {location: 'replace'});
        });
        $rootScope.$on('$viewContentLoaded', function () {
        });
        $rootScope.$on('$stateChangeSuccess', function () {

        });
    }]);
	