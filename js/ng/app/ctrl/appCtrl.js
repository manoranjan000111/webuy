ngApp.controller('appCtrl', ['$scope', '$window', '$filter', '$state', '$stateParams', 'ngTableParams', 'fileUploadSvc', 'SERVICE', function ($scope, $window, $filter, $state, $stateParams, ngTableParams, fileUploadSvc, SERVICE) {
        $scope.$on('$viewContentLoaded', function () {
            $scope.showForm = !1;
            $scope.showListing = $scope.sbhide = $scope.nbhide = $scope.loading = 1;
            $scope.listingRec = [];
            $scope.frmobj = $scope.bnk = $scope.frmdata = {};
            $scope.buttonvalue = $scope.formaction = '';
            $scope.tableParams = new ngTableParams({debugMode: !1, page: 1, count: 100}, {
                total: $scope.listingRec.length, getData: function (e, t) {
                    var o = t.filter() ? $filter("filter")($scope.listingRec, t.filter()) : $scope.listingRec, a = t.sorting() ? $filter("orderBy")(o, t.orderBy()) : o;
                    $scope.orderedData = a, t.total(a.length), t.settings({counts: o.length > 50 ? [50, 100, 200, 300, 500, 1000] : []}), e.resolve($scope.data = a.slice((t.page() - 1) * t.count(), t.page() * t.count()))
                }
            });
            $scope.getCategory = function () {
                $scope.uloading = 1;
                $scope.categoryData = [];
                SERVICE.get({c: 'weBuy', m: 'getCategory'}).$promise.then(function (response) {
                    $scope.categoryData = response.res;
                    $scope.uloading = !1;
                });
            };
            $scope.getProduct = function () {
                $scope.uloading = 1;
                $scope.prodctData = [];
                SERVICE.get({c: 'weBuy', m: 'getProduct'}).$promise.then(function (response) {
                    $scope.prodctData = response.res;
                    $scope.categoryList = response.catlist;
                    $scope.uloading = !1;
                });
            };
            $scope.getProductList = function () {
                $scope.uloading = 1;
                $scope.prodctData = [];
                SERVICE.get({c: 'weBuy', m: 'getProductList'}).$promise.then(function (response) {
                    $scope.prodctData = response.res;
                    $scope.uloading = !1;
                });
            };
            $scope.getOrder = function () {
                $scope.uloading = 1;
                $scope.orderData = [];
                SERVICE.get({c: 'weBuy', m: 'getOrder'}).$promise.then(function (response) {
                    $scope.orderData = response.res;
                    $scope.uloading = !1;
                });
            };
            $scope.getCart = function () {
                $scope.uloading = 1;
                $scope.cartData = [];
                SERVICE.get({c: 'weBuy', m: 'getCartData'}).$promise.then(function (response) {
                    $scope.cartData = response.res;
                    $scope.uloading = !1;
                });
            };
            $scope.deliverPrs = function () {
                $scope.dlist = [
                    {key: 'BUY', val: 'BUY'},
                    {key: 'SHIP', val: 'SHIP'},
                    {key: 'DELIVER', val: 'DELIVER'},
                    {key: 'RETURN', val: 'RETURN'},
                    {key: 'CANCEL', val: 'CANCEL'},
                ];
            };
            $scope.getUserData = function () {
                $scope.uloading = 1;
                $scope.userData = [];
                SERVICE.get({c: 'weBuy', m: 'getUserData'}).$promise.then(function (response) {
                    $scope.userData = response.res;
                    $scope.uloading = !1;
                });
            };
            $scope.alertpopup = function (response) {
                if (response.status == true) {
                    bootbox.alert({title: response.title, message: succ + response.msg + clp, size: 'small', backdrop: true});
                } else {
                    bootbox.alert({title: response.title, message: err + response.msg + clp, size: 'small', backdrop: true});
                }
            };
            $scope.cancelHide = function () {
                $scope.frmobj = $scope.bnk = $scope.frmdata = {};
                $scope.listingRec = [];
                $scope.showform = !1;
                $scope.showListing = $scope.nbhide = $scope.sbhide = 1;
                $scope.ngdisabled = 'new';
                $scope.formaction = '';
                $scope.initApp();
            };
            $scope.newRec = function (opt) {
                $scope.showListing = $scope.submitted = !1;
                $scope.showForm = $scope.sbhide = 1;
                $scope.frmobj = $scope.bnk = $scope.frmdata = {};
                $scope.errmsg = '';
                $scope.ngdisabled = $scope.formaction = 'create';
                $scope.buttonvalue = 'Save';
                angular.element('.msubmit').prop("disabled", false);
                switch ($state.current.name.toLowerCase()) {
                    case 'product':
                        $scope.bnk.cgid = $scope.categoryList[0];
                    default:
                        break;
                }
            };
            $scope.editRec = function (b, flag) {
                $scope.showListing = $scope.submitted = !1;
                $scope.showForm = $scope.sbhide = 1;
                $scope.formaction = flag;
                $scope.errmsg = '';
                $scope.bnk = b;
                angular.element('.msubmit').prop("disabled", false);
                switch (flag) {
                    case 'view':
                        $scope.ngdisabled = 'view';
                        $scope.buttonvalue = 'Update';
                        $scope.sbhide = !1;
                        break;
                    case 'edit':
                        $scope.ngdisabled = 'edit';
                        $scope.buttonvalue = 'Update';
                        break;
                    case 'delete':
                        $scope.ngdisabled = 'view';
                        $scope.buttonvalue = 'Delete';
                        break;
                    default:
                        break;
                }
                switch ($state.current.name.toLowerCase()) {
                    case 'orderlist':
                        $scope.bnk.deliveropt = b.deliver;
                        angular.forEach($scope.dlist, function (v, i) {
                            if (b.deliver === v.key) {
                                $scope.bnk.deliver = v;
                            }
                        });
                        break;
                    case 'product':
                        angular.forEach($scope.categoryList, function (v, i) {
                            if (b.cgid === v.cgid) {
                                $scope.bnk.cgid = v;
                            }
                        });
                        break;
                    default:
                        break;
                }
            };
            $scope.saveRec = function (frm) {
                $scope.submitted = 1;
                $scope.errmsg = '';
                if (frm.$valid === true) {
                    objDB = new SERVICE({c: 'weBuy', m: 'POST'});
                    objDB.formaction = $scope.formaction;
                    objDB.action = $scope.action;
                    objDB.frmdata = $scope.bnk;
                    angular.element('.msubmit').prop("disabled", true);
                    objDB.$save(function (response) {
                        if (response.status === true) {
                            $scope.submitted = $scope.showform = !1;
                            $scope.cancelHide();
                        } else if (response.status === false) {
                            $scope.errmsg = response.msg;
                        } else {
                            $scope.errmsg = 'something went worng!';
                        }
                        $scope.alertpopup(response);
                    });
                } else {
                    angular.element('.msubmit').prop("disabled", false);
                    $scope.errmsg = "Please fill all valid information.";
                }
            };
            $scope.initApp = function () {
                $scope.frmdata = {};
                $scope.showForm = !1;
                $scope.showListing = $scope.Loading = 1;
                $scope.action = $state.current.name.toLowerCase();
                $scope.getArray = [];
                if (localStorage.active == 1) {
                    switch ($scope.action) {
                        case 'loading':
                            if (localStorage.ut == 'CMS') {
                                $state.go('orderlist', {}, {location: 'replace'});
                            } else {
                                $state.go('products', {}, {location: 'replace'});
                            }
                            break;
                        case 'dashboard':
                            break;
                        case 'category':
                            $scope.getCategory();
                            break;
                        case 'product':
                            $scope.getProduct();
                            break;
                        case 'products':
                            $scope.getProductList();
                            break;
                        case 'orderlist':
                            $scope.getOrder();
                            $scope.deliverPrs();
                            break;
                        case 'cartlist':
                            $scope.getCart();
                            break;
                        case 'userlist':
                            $scope.getUserData();
                            break;
                        default:
                            $state.go('404', {}, {location: 'replace'});
                            break;
                    }
                } else {
                    $window.location.href = projectDir;
                }
            };
            $scope.initApp();
            $scope.buyNow = function (d) {
                bootbox.confirm({
                    title: "Destroy planet?",
                    message: "Do you want to activate the Deathstar now? This cannot be undone.",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirm'
                        }
                    },
                    callback: function (action) {
                        if (action == true) {
                            objDB = new SERVICE({c: 'weBuy', m: 'POST'});
                            objDB.frmdata = d.pid;
                            objDB.action = 'buyNow';
                            objDB.formaction = 'create';
                            objDB.$save(function (response) {
                                $scope.getCart();
                                $scope.alertpopup(response);
                            });
                        }
                    }
                });
            };
            $scope.addToCart = function (d) {
                objDB = new SERVICE({c: 'weBuy', m: 'POST'});
                objDB.frmdata = d.pid;
                objDB.action = 'addToCart';
                objDB.formaction = 'create';
                objDB.$save(function (response) {
                    $scope.alertpopup(response);
                });
            };
            $scope.removeFromCart = function (d) {
                objDB = new SERVICE({c: 'weBuy', m: 'POST'});
                objDB.frmdata = d.pid;
                objDB.action = 'removeFromCart';
                objDB.formaction = 'delete';
                objDB.$save(function (response) {
                    $scope.getCart();
                    $scope.alertpopup(response);
                });
            };
        });
    }]);