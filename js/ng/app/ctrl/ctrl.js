ngApp.controller('authctrl', ['$scope', '$window', 'auth', function ($scope, $window, auth) {
        $scope.user = {};
        $scope.switch_val = 'login';
        $scope.logout = function () {
            localStorage.removeItem("active");
            localStorage.removeItem("ut");
            localStorage.removeItem("uid");
            localStorage.removeItem("uname");
            let obj = new auth();
            obj.action = 'logout';
            obj.$save(function () {
                $window.location.href = projectDir;
            });
        };
        $scope.signInSignUp = function (v) {
            $scope.switch_val = v
            $scope.errmsg = '';
            $scope.submitted = !1;
        };
        $scope.init = function () {
            $scope.usernmae = localStorage.uname;
            $scope.usertype = localStorage.ut;
            if (typeof (Storage) !== "undefined") {
                if (localStorage.active == 1) {
                    $window.location.href = projectDir + '/home.html#!/loading';
                } else {
                    if (window.location.href.indexOf('home') > 0) {
                        $window.location.href = projectDir;
                    }
                }
            } else {
                alert("Oops.kindly update your browser verson!");
            }
        };
        $scope.setAppSession = function (data) {
            if (typeof (Storage) !== "undefined") {
                localStorage.active = 1;
                localStorage.token = data.token;
                localStorage.ut = data.ut;
                localStorage.uid = data.uid;
                localStorage.uname = data.uname;
                $window.location.href = projectDir + '/home.html#!/loading';
            } else {
                alert("Oops.kindly update your browser verson!");
            }
        };
        $scope.doLogin = function () {
            let obj = new auth();
            obj.userdata = $scope.user;
            obj.action = 'doLogin';
            angular.element('.showhide').css('display', 'block');
            obj.$save(function (response) {
                $scope.loginprs = false;
                angular.element('.showhide').css('display', 'none');
                if (response.status === 'success') {
                    angular.element('.msubmit').prop("disabled", false);
                    angular.element('#logon').css({"background-color": "rgba('0,0,0,0.5')", "z-index": "2", "cursor": "pointer"});
                    $scope.user = {};
                    $scope.submitted = false;
                    $scope.errmsg = '';
                    angular.element('.errmsgauthenticating').html('Authenticating...');
                    $scope.setAppSession(response.res);
                    angular.element('.redirect').html('Processing...<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
                    angular.element('.redirect').css({"color": "green"}).fadeIn(1000).fadeOut(5000);
                    $scope.loginprs = true;
                    $scope.errmsg = '';
                } else {
                    angular.element('#logon').css('display', 'block');
                    angular.element('#otp').css('display', 'none');
                    angular.element('.optbox').css('display', 'none');
                    $scope.optdiv = !1;
                    $scope.submitted = true;
                    $scope.loginprs = false;
                    $scope.errmsg = response.error_message;
                }
                angular.element('.msubmit').prop("disabled", false);
            });
        };
        $scope.doSignup = function (frm) {
            $scope.submitted = 1;
            $scope.errmsg = '';
            if (frm.$valid === true) {
                objDB = new auth();
                objDB.formaction = 'create';
                objDB.action = 'doSignup'
                objDB.userdata = $scope.user;
                angular.element('.msubmit').prop("disabled", true);
                objDB.$save(function (response) {
                    if (response.status === true) {
                        $scope.submitted = $scope.showform = !1;
                        $scope.errmsg = response.msg;
                        $scope.signInSignUp('login');
                    } else if (response.status === false) {
                        angular.element('.msubmit').prop("disabled", false);
                        $scope.errmsg = response.msg;
                    } else {
                        angular.element('.msubmit').prop("disabled", false);
                        $scope.errmsg = 'something went worng!';
                    }
                });
            } else {
                angular.element('.msubmit').prop("disabled", false);
                $scope.errmsg = "Please fill all valid information.";
            }
        };
        $scope.init();

    }]);