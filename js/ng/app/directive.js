ngApp.directive('dateInput', function () {
    return {
        restrict: 'A',
        scope: {
            ngModel: '='
        },
        link: function (scope) {
            if (scope.ngModel)
                scope.ngModel = new Date(scope.ngModel);
        }
    }
});
ngApp.directive('format', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl)
                    return;
                ctrl.$formatters.unshift(function (a) {
                    return $filter(attrs.format)(ctrl.$modelValue)
                });
                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.format)(plainNumber));
                    return plainNumber;
                });
            }
        };
    }]);
ngApp.directive('number', ['$parse', function ($parse) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelController) {
                ngModelController.$parsers.push(function (data) {
                    return parseInt(data);
                });
                ngModelController.$formatters.push(function (data) {
                    if (data) {
                        var model = $parse(attrs['ngModel']);
                        model.assign(scope, parseInt(data));
                    }
                    return parseInt(data);

                });
            }
        }
    }]);
ngApp.directive("compareTo", function () {
    return {
        require: "ngModel",
        scope: {repeatPassword: "=compareTo"
        },
        link: function (scope, element, attributes, paramval) {
            paramval.$validators.compareTo = function (val) {
                return val === scope.repeatPassword;
            };
            scope.$watch("repeatPassword", function () {
                paramval.$validate();
            });
        }
    };
});  