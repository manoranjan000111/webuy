ngApp.filter('dateTimeFilter', function ($filter) {
    return function (input) {
        if (input == null || input == '0000-00-00 00:00:00' || input == '') {
            return "";
        }
        var _date = $filter('date')(new Date(input), 'dd-MM-yyyy HH:mm:ss');
        return _date.toUpperCase();
    };
});
ngApp.filter('ellipses', function () {
    return function (value, wordwise, max, tail) {
        if (!value)
            return '';
        max = parseInt(max, 10);
        if (!max)
            return value;
        if (value.length <= max)
            return value;
        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace !== -1) {
                if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }
        return value + (tail || '…');
    };
});