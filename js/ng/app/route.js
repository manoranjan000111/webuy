ngApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider
                .state('404', {
                    url: "/404",
                    templateUrl: "views/404.html",
                    data: {pageTitle: 'Page Not Found'},
                    controller: "appCtrl",
                })
                .state('loading', {
                    url: "/loading",
                    templateUrl: "views/loading.html",
                    controller: "appCtrl",
                })
                .state('dashboard', {
                    url: "/dashboard",
                    templateUrl: "views/sample.html",
                    controller: "appCtrl",
                })
                .state('category', {
                    url: "/category",
                    templateUrl: "views/webuy/category.html",
                    controller: "appCtrl",
                })
                .state('product', {
                    url: "/product",
                    templateUrl: "views/webuy/product.html",
                    controller: "appCtrl",
                })
                .state('products', {
                    url: "/products",
                    templateUrl: "views/webuy/products.html",
                    controller: "appCtrl",
                })
                .state('orderlist', {
                    url: "/orderlist",
                    templateUrl: "views/webuy/order.html",
                    controller: "appCtrl",
                })
                .state('userlist', {
                    url: "/userlist",
                    templateUrl: "views/webuy/user.html",
                    controller: "appCtrl",
                })
                .state('cartlist', {
                    url: "/cartlist",
                    templateUrl: "views/webuy/cart.html",
                    controller: "appCtrl",
                })
    }]);
