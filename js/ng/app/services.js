ngApp.service('fileUploadSvc', function ($http, $q, ) {
    this.upload = function (formid, uploadUrl, data) {
        var fd = new FormData(document.getElementById(formid));
        fd.append('post', JSON.stringify(data));
        var deferred = $q.defer();
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            if (response.status == 200) {
                deferred.resolve(response.data);
            } else {
                deferred.reject();
            }
        });
        return deferred.promise;
    }
});
ngApp.service('todaydate', function () {
    this.gettodaydate = function (format) {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return today = dd + format + mm + format + yyyy;
    };
});
ngApp.service('cstmSvc', function ($http, $q, $filter) {
    this.upload = function (formid, uploadUrl, data) {
        var fd = new FormData(document.getElementById(formid));
        fd.append('post', JSON.stringify(data));
        var deferred = $q.defer();
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
                .success(function (response) {
                    deferred.resolve(response);
                })
                .error(function () {
                    deferred.reject(reject);
                });
        return deferred.promise;
    }
    this.tablekey1 = function ($scope) {
        return {debugMode: false, page: 1, count: tbllist, filter: $scope.filter};
    }
    this.tablecall = function ($defer, params, $scope, listing) {
        var filteredData = params.filter() ? $filter('filter')(listing, params.filter().term) : listing;
        var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
        $scope.orderedData = orderedData;
        params.total(orderedData.length);
        params.settings({counts: filteredData.length > tbllist ? [5, 10, 20, 50, 100, 200, 500] : []});
        $scope.data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
        $defer.resolve($scope.data)
    }
});

