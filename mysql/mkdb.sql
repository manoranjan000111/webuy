
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE IF NOT EXISTS `mkdb`;


DROP TABLE IF EXISTS `mk_cart`;
CREATE TABLE `mk_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(16) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `qty` tinyint(4) NOT NULL DEFAULT 1,
  `cid` varchar(30) DEFAULT NULL,
  `cdt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `mid` varchar(30) DEFAULT NULL,
  `mdt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mk_cart` (`id`, `uid`, `pid`, `qty`, `cid`, `cdt`, `mid`, `mdt`) VALUES
(1,	'19121002',	1001,	1,	'19121002',	'2019-12-29 11:12:48',	NULL,	NULL),
(4,	'19121003',	1007,	1,	'19121003',	'2019-12-29 11:31:43',	NULL,	NULL),
(5,	'19121002',	1002,	1,	'19121002',	'2019-12-29 11:32:59',	NULL,	NULL);

DROP TABLE IF EXISTS `mk_category`;
CREATE TABLE `mk_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cgid` int(11) DEFAULT NULL,
  `ctype` varchar(32) NOT NULL DEFAULT 'OTHER',
  `cdesc` varchar(64) DEFAULT NULL,
  `rstatus` enum('A','X') NOT NULL DEFAULT 'A',
  `cid` varchar(30) DEFAULT NULL,
  `cdt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `mid` varchar(30) DEFAULT NULL,
  `mdt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `cid` (`cgid`),
  KEY `cid_ctype` (`cgid`,`ctype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mk_category` (`id`, `cgid`, `ctype`, `cdesc`, `rstatus`, `cid`, `cdt`, `mid`, `mdt`) VALUES
(1,	1001,	'GAMES',	'In Door And Out Door Games',	'A',	'ADMIN',	'2019-12-29 10:40:20',	'U19120001',	'2019-12-29 10:40:20'),
(2,	1002,	'MOVIES',	'Bollywood Movies',	'A',	'ADMIN',	'2019-12-29 10:38:12',	NULL,	NULL),
(3,	1003,	'ELECTRONICS',	'Electronics',	'A',	'ADMIN',	'2019-12-29 10:38:56',	NULL,	NULL),
(4,	1004,	'FITNESS',	'Yoga',	'A',	'ADMIN',	'2019-12-29 10:50:30',	NULL,	NULL),
(5,	1005,	'HOME',	'Home Improvement',	'A',	'ADMIN',	'2019-12-29 11:02:30',	NULL,	NULL);

DROP TABLE IF EXISTS `mk_order`;
CREATE TABLE `mk_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordid` varchar(16) DEFAULT NULL,
  `uid` varchar(32) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `pay` enum('COD','PAID') NOT NULL DEFAULT 'COD',
  `pamt` decimal(10,2) DEFAULT 1.00,
  `deliver` enum('BUY','SHIP','DELIVER','RETURN','CANCEL') NOT NULL DEFAULT 'BUY',
  `rstatus` enum('A','X') NOT NULL DEFAULT 'A',
  `cid` varchar(30) DEFAULT NULL,
  `cdt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `mid` varchar(30) DEFAULT NULL,
  `mdt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `ordid` (`ordid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mk_order` (`id`, `ordid`, `uid`, `pid`, `pay`, `pamt`, `deliver`, `rstatus`, `cid`, `cdt`, `mid`, `mdt`) VALUES
(1,	'19121001',	'19121003',	1002,	'COD',	299.00,	'SHIP',	'A',	'19121003',	'2019-12-29 11:42:35',	'U19120001',	'2019-12-29 11:42:35'),
(2,	'19121002',	'19121003',	1005,	'COD',	209.00,	'BUY',	'A',	'19121003',	'2019-12-29 11:31:59',	NULL,	NULL),
(3,	'19121003',	'19121003',	1003,	'COD',	799.00,	'BUY',	'A',	'19121003',	'2019-12-29 11:32:15',	NULL,	NULL);

DROP TABLE IF EXISTS `mk_product`;
CREATE TABLE `mk_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `pcode` varchar(32) DEFAULT NULL,
  `ptitle` varchar(32) DEFAULT NULL,
  `pprice` decimal(10,2) NOT NULL DEFAULT 1.00,
  `cgid` int(11) DEFAULT NULL,
  `pdesc` varchar(200) DEFAULT NULL,
  `img` varchar(8) DEFAULT NULL,
  `rstatus` enum('A','X') NOT NULL DEFAULT 'A',
  `cid` varchar(30) DEFAULT NULL,
  `cdt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `mid` varchar(30) DEFAULT NULL,
  `mdt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `pcode` (`pcode`),
  UNIQUE KEY `pid` (`pid`),
  KEY `pcode_cid` (`pcode`,`cgid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mk_product` (`id`, `pid`, `pcode`, `ptitle`, `pprice`, `cgid`, `pdesc`, `img`, `rstatus`, `cid`, `cdt`, `mid`, `mdt`) VALUES
(1,	1001,	'FITN0001',	'Yoga Mat',	200.00,	1004,	'Nova 100 EVA Eco Friendly Non Slip Blue 6 Mm Yoga Mat',	NULL,	'A',	'ADMIN',	'2019-12-29 11:44:01',	'U19120001',	'2019-12-29 11:44:01'),
(2,	1002,	'PC1002',	'Dendrobium Orchid Live Plant',	299.00,	1005,	'OrchidWala Dendrobium Orchid Live Plant Plant Hybrid Pack Of 1 Flower',	NULL,	'A',	'ADMIN',	'2019-12-29 11:05:26',	NULL,	NULL),
(3,	1003,	'PC1003',	'TrustBasket Plant Container  (Me',	799.00,	1005,	'FLOWERSPLANTS AND POTS ARE NOT INCLUDED PRODUCT DIMENSION Product Height - 95 Cm Product Breadth - 225 Cm Product Length - 61 Cm',	NULL,	'A',	'ADMIN',	'2019-12-29 11:09:59',	NULL,	NULL),
(4,	1004,	'PC1004',	'Philips QT3310/15 Runtime: 30 mi',	1099.00,	1003,	'With Stainless Steel Blades That Sharpen Themselves And Be Adjusted To Trim Different Length Hair This Philips Hair Trimmer',	NULL,	'A',	'ADMIN',	'2019-12-29 11:21:02',	NULL,	NULL),
(5,	1005,	'PC1005',	'Mattel Games Fun Employed  (Mult',	209.00,	1001,	'Funemployed Is The Hilarious Card-Based Party Game For Adults That Rewards Creativity As Players Try To Be Convincing Job Candidates',	NULL,	'A',	'ADMIN',	'2019-12-29 11:25:16',	NULL,	NULL),
(7,	1006,	'PC1006',	'Avengers Endgame Infinity War',	250.00,	1002,	'Avengers Infinity War  Thor Ragnarok  Avengers Age Of Ultron  Justice League The Avengers  Ant-Man And The Wasp Jurassic World Fallen Kingdom Dual Audio Hindi And English',	NULL,	'A',	'ADMIN',	'2019-12-29 11:28:48',	NULL,	NULL),
(8,	1007,	'PC1007',	'Captain America - The Winter Sol',	500.00,	1002,	'Captain America - The Winter Soldier DVD English',	NULL,	'A',	'ADMIN',	'2019-12-29 11:29:41',	NULL,	NULL),
(9,	1008,	'PC1008',	'Bajaj GX1 500 W Mixer Grinder',	2000.00,	1003,	'Bajaj Mixer Grinder Has Got You Covered With The Stainless Steel Jars And Their Easy-grip Handles This Grinder Ensures A Hassle-free Grinding Experience',	NULL,	'A',	'ADMIN',	'2019-12-29 11:31:13',	NULL,	NULL);

DROP TABLE IF EXISTS `mk_user`;
CREATE TABLE `mk_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(16) DEFAULT NULL,
  `ucode` varchar(32) DEFAULT NULL,
  `upass` varchar(100) DEFAULT NULL,
  `utype` varchar(8) NOT NULL DEFAULT 'F',
  `urole` varchar(8) NOT NULL DEFAULT '0',
  `title` enum('Mr.','Mrs.','Miss','OTM') NOT NULL DEFAULT 'Mr.',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(62) DEFAULT NULL,
  `mobile` varchar(16) DEFAULT NULL,
  `gender` enum('M','F') NOT NULL DEFAULT 'M',
  `address` varchar(120) DEFAULT NULL,
  `rstatus` enum('A','X') NOT NULL DEFAULT 'A',
  `cid` varchar(30) DEFAULT NULL,
  `cdt` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `mid` varchar(30) DEFAULT NULL,
  `mdt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `pid` (`uid`),
  UNIQUE KEY `ucode` (`ucode`),
  KEY `pcode_cid` (`ucode`,`utype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `mk_user` (`id`, `uid`, `ucode`, `upass`, `utype`, `urole`, `title`, `name`, `email`, `mobile`, `gender`, `address`, `rstatus`, `cid`, `cdt`, `mid`, `mdt`) VALUES
(1,	'U19120001',	'ADMIN',	'21232f297a57a5a743894a0e4a801fc3',	'CMS',	'S',	'Mr.',	'Admin',	'admin@gmail.com',	'9999999990',	'M',	' Mumbai',	'A',	'U19120001',	'2019-12-29 10:34:26',	NULL,	NULL),
(2,	'19121002',	'MANORANJAN',	'31b02ec8a4ff3869b380538c70cb6764',	'F',	'0',	'Mr.',	'Manoranjan',	'manoranjan000111@gmail.com',	'8825227372',	'M',	'Mumabi',	'A',	'',	'2019-12-29 10:55:15',	NULL,	NULL),
(3,	'19121003',	'RANJAN',	'a7cd2205b8031f5858acdfb9eb7d9952',	'F',	'0',	'Mr.',	'Ranjan',	'ranjan@yopmail.com',	'730495521',	'M',	'Pune',	'A',	'',	'2019-12-29 11:13:44',	NULL,	NULL);
