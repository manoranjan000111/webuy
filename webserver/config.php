<?php
session_start();
switch ($_SERVER['SERVER_NAME']) {
    case 'localhost':
    case '127.0.0.1':
        include 'dbconnection/localhost.php';
        break;
    default:
        echo json_encode(array('status' => 1, 'error_message' => 'Unable to conect database...','data'=>'','res'=>'', 'server' =>$_SERVER['SERVER_NAME']));
        die;
}