<?php

/**
 * Description of authController
 *
 * @author Manoranjan Kumar
 * Date: 29-12-2019
 */
class authController {

    public $now;
    public $today;
    public $uid;

    function __construct() {
        $this->now = date('Y-m-d H:i:s');
        $this->today = date('Y-m-d');
        $this->uid = isset($_SESSION['SESS_USERID']) ? $_SESSION['SESS_USERID'] : '';
    }

    function GET() {
        
    }

    function POST() {
        $params = json_decode((isset($_POST['post']) ? $_POST['post'] : file_get_contents('php://input')), true);
        switch (strtolower($params['action'])) {
            case 'dologin':$this->doLogin($params['userdata']);
                break;
            case 'dosignup':$this->doSignup($params['userdata']);
                break;
            case 'logout':$this->logout();
                break;
            default:
                break;
        }
    }

    function doLogin($param) {
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_PARSE);
        $password = (isset($param["password"])) ? trim($param["password"]) : null;
        $userid = (isset($param["username"])) ? strtoupper(trim($param["username"])) : null;
        if ($userid != '') {
            $data = array('userid' => $userid, 'password' => $password);
            switch (strtolower($_SERVER['SERVER_NAME'])) {
                case 'localhost':
                case '127.0.0.1':
                    $this->login($data);
                    break;
                default :
                    echo json_encode(array("status" => 1, 'error_message' => 'Oops. something went wrong.'));
                    die;
                    break;
            }
        } else {
            echo json_encode(array('status' => 1, 'error_message' => 'Please check login credentials.'));
            die;
        }
    }

    function doSignup($param) {
        $name = (isset($param["name"])) ? ucwords(trim($param["name"])) : null;
        $address = (isset($param["address"])) ? ucwords(trim($param["address"])) : null;
        $mobile = (isset($param["mobile"])) ? trim($param["mobile"]) : null;
        $email = (isset($param["email"])) ? strtolower(trim($param["email"])) : null;
        $usercode = (isset($param["usercode"])) ? strtoupper(trim($param["usercode"])) : null;
        $userpass = (isset($param["userpass"])) ? trim($param["userpass"]) : null;
        $confirm = (isset($param["confirm"])) ? trim($param["confirm"]) : null;
        if ($userpass != '' && $email != '' && $usercode != '' && $mobile != '') {
            if ($userpass == $confirm) {
                $res = so("SELECT id FROM mk_user ORDER BY id DESC LIMIT 1");
                $ym = date('ym');
                $uid = '1000' + 1;
                $uid = $ym . $uid;
                if ($res != '') {
                    $uid = $res['id'] + 1;
                    $uid = '1000' + $uid;
                    $uid = $ym . $uid;
                }
                $userpass = md5($userpass);
                $q = "INSERT INTO mk_user (uid, ucode, upass,utype, urole,name, email, mobile,address, cid, cdt) VALUES ('{$uid}', '{$usercode}','{$userpass}', 'F', '0','{$name}', '{$email}', '{$mobile}','{$address}', '{$pid}', now())";
                $rep = i($q);
                if ($rep['errorInfo'][0] == '00000') {
                    $retrunarr = array('status' => TRUE, 'msg' => 'Created successfully.');
                } else {
                    $retrunarr = array('status' => FALSE, 'msg' => 'The username already exists. Please use a different username.');
                }
                echo json_encode($retrunarr);
                die;
            } else {
                echo json_encode(array('status' => FALSE, 'msg' => 'Oops. Password and Confirm Password not matching.'));
                die;
            }
        } else {
            echo json_encode(array('status' => FALSE, 'msg' => 'Please fill all information.'));
            die;
        }
    }

    public function login($param) {
        $userid = $param['userid'];
        $userpass = md5($param['password']);
        $sql = "SELECT uid, ucode, utype, urole, title, name, mobile, mobile, id FROM mk_user WHERE ucode = '{$userid}' AND upass = '{$userpass}' LIMIT 1";
        $res = so($sql);
        if ($res != '' && count($res) > 0) {
            $rep = array(
                'token' => time() . '#' . 'ADMIN',
                'uid' => $res['uid'],
                'ut' => $res['utype'],
                'uname' => $res['name'],
            );
            $this->setsession($rep);
            echo json_encode(array("status" => "success", "res" => $rep, 'error_message' => ""));
        } else {
            echo json_encode(array("status" => "error", "error_message" => "Login failed: Invalid username or password", "res" => []));
        }
    }

    public function logout() {
        $_SESSION = [];
        session_unset();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
            );
        }
        session_destroy();
        echo json_encode(array("status" => "success", "result" => []));
    }

    public function setsession($param) {
        $_SESSION["SESSUSERID"] = $param["uid"];
        $_SESSION["SESS_USERID"] = $param["uid"];
        $_SESSION["SESS_TOKEN"] = $param["token"];
        $_SESSION["UT"] = $param["ut"];
    }

    public function userinfo() {
        echo json_encode($_SESSION);
    }

    function validateSession() {
        if (!isset($_SESSION["SESS_USERID"]) || $_SESSION["SESS_STATUS"] != TRUE) {
            echo json_encode(array("status" => "error", "result" => "Invalid Session"));
        } else {
            echo json_encode(array("status" => "success", "result" => $_SESSION));
        }
    }

}
