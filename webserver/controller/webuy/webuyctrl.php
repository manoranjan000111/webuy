<?php

/**
 * Description of weBuy
 * weBuy Reports
 * @author Manoranjan Kumar
 * Date: 29-12-2019
 */
class weBuy {

    private $user;
    private $now;
    private $today;

    function __construct() {
        $this->user = isset($_SESSION['SESS_USERID']) ? $_SESSION['SESS_USERID'] : '';
        $this->now = date('Y-m-d H:i:s');
        $this->today = date('Y-m-d');
    }

    function GET() {
        
    }

    function POST() {
        $params = json_decode(file_get_contents('php://input'), true);
        switch (strtolower($params['action'])) {
            case 'category':
                $this->categoryCRUD($params['frmdata'], $params['formaction']);
                break;
            case 'product':
                $this->productCRUD($params['frmdata'], $params['formaction']);
                break;
            case 'addtocart':
                $this->addToCart($params['frmdata'], $params['formaction']);
                break;
            case 'removefromcart':
                $this->removeFromCart($params['frmdata'], $params['formaction']);
                break;
            case 'buynow':
                $this->buyNow($params['frmdata'], $params['formaction']);
                break;
            case 'orderlist':
                $this->updateOrder($params['frmdata'], $params['formaction']);
                break;
            default:
                break;
        }
    }

    function DELETE() {
        
    }

    function getCategory($return = false) {
        if ($return == TRUE) {
            $sql = "SELECT id,cgid,ctype,cdesc FROM mk_category WHERE rstatus = 'A' ORDER BY id DESC";
            return sa($sql);
        } else {
            $q = [];
            $sql = "SELECT * FROM mk_category WHERE rstatus = 'A' ORDER BY id DESC";
            $q = sa($sql);
            echo json_encode(array('status' => 'success', "res" => $q));
        }
    }

    function getProduct($return = false) {
        if ($return == TRUE) {
            $sql = "SELECT p.pid,
                            p.pcode,
                            p.ptitle,
                            p.pprice,
                            p.pdesc,
                            p.cgid,
                            c.ctype,
                            c.cdesc
                     FROM mk_product p
                     INNER JOIN mk_category as c on p.cgid =c.cgid
                     WHERE p.rstatus='A'
                     ORDER BY p.id DESC";
            return sa($sql);
        } else {
            $q = [];
            $sql = "SELECT p.*,
                            c.ctype,
                            c.cdesc
                     FROM mk_product p
                     INNER JOIN mk_category as c on p.cgid =c.cgid
                     WHERE p.rstatus='A'
                     ORDER BY p.id DESC";
            $q = sa($sql);
            echo json_encode(array('status' => 'success', "res" => $q,'catlist'=> $this->getCategory(TRUE)));
        }
    }
    
    function getProductList() {
        echo json_encode(array('status' => 'success','res' => $this->getProduct(TRUE)));
    }

    function getOrder() {
        $q = [];
        $sql = "SELECT o.*, 
                        p.pid, 
                        p.pcode, 
                        p.cgid, 
                        p.pdesc, 
                        u.uid, 
                        u.ucode, 
                        u.title, 
                        u.name,
                        u.email, 
                        u.mobile, 
                        u.gender 
                 FROM   mk_order AS o 
                        INNER JOIN mk_user AS u 
                                ON u.uid = o.uid 
                        INNER JOIN mk_product AS p 
                                ON p.pid = o.pid 
                 WHERE  o.rstatus = 'A' 
                 ORDER  BY o.id DESC ";
        $q = sa($sql);
        echo json_encode(array('status' => 'success', "res" => $q));
    }
    
    function getCartData() {
        $q = [];
        $sql = "SELECT c.*, 
                        p.pid, 
                        p.pcode, 
                        p.cgid, 
                        p.pdesc, 
                        p.ptitle, 
                        p.pprice,
                        ct.ctype,
                        ct.cdesc,
                        ct.cgid
                 FROM   mk_cart AS c 
                        INNER JOIN mk_product AS p 
                                ON p.pid = c.pid 
                        INNER JOIN mk_category AS ct
                                ON p.cgid = ct.cgid
                 WHERE c.cid='{$this->user}'        
                 ORDER  BY c.id DESC ";
        $q = sa($sql);
        echo json_encode(array('status' => 'success', "res" => $q));
    }

    function getUserData() {
        $q = [];
        $sql = "SELECT u.*
                 FROM   mk_user AS u
                 WHERE  u.rstatus = 'A' 
                 ORDER  BY u.id DESC ";
        $q = sa($sql);
        echo json_encode(array('status' => 'success', "res" => $q));
    }

    function cleanString($string) {
        $string = preg_replace('!\s+!', ' ', trim($string));
        return preg_replace('/[^A-Za-z0-9\- ]/', '', $string);
    }

    function categoryCRUD($param, $formaction) {
        $ctype = isset($param['ctype']) ? substr(strtoupper($this->cleanString($param['ctype'])), 0, 30) : '';
        $cdesc = isset($param['cdesc']) ? substr(ucwords($this->cleanString($param['cdesc'])), 0, 60) : '';
        switch (strtolower($formaction)) {
            case 'create':
            case 'new':
                if ($ctype != "" && $cdesc != "") {
                    $res = so("SELECT cgid FROM   mk_category ORDER  BY cgid DESC LIMIT 1");
                    $cgid = '1000' + 1;
                    if ($res != '') {
                        $cgid = $res['cgid'] + 1;
                    }
                    $q = "INSERT INTO `mk_category` (`cgid`, `ctype`, `cdesc`, `rstatus`, `cid`, `cdt`, `mid`, `mdt`) VALUES ('{$cgid}', '{$ctype}', '{$cdesc}', 'A', 'ADMIN', now(), NULL, NULL)";
                    $rep = i($q);
                    if ($rep['errorInfo'][0] == '00000') {
                        $retrunarr = array('status' => TRUE, 'msg' => 'Created successfully :' . $cgid, 'title' => 'Category', 'res' => array('id' => $cgid));
                    } else {
                        $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Category', 'res' => array('id' => ''));
                    }
                } else {
                    $retrunarr = array('status' => FALSE, 'msg' => 'Fill all information.', 'title' => 'Category', 'res' => array('id' => ''));
                }
                echo json_encode($retrunarr);
                die;
                break;
            case 'edit':
            case 'update':
                $rep = u("UPDATE mk_category SET cdesc = '{$cdesc}',ctype = '{$ctype}',mid='{$this->user}' ,mdt=now() WHERE id = '{$param['id']}'");
                if ($rep['errorInfo'][0] == '00000') {
                    $retrunarr = array('status' => TRUE, 'msg' => 'Updated successfully :' . $param['cgid'], 'title' => 'Category', 'res' => array('id' => $param['cgid']));
                } else {
                    $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Category', 'res' => array('id' => ''));
                }
                echo json_encode($retrunarr);
                die;
                break;
            case 'delete':
                //d("DELETE FROM mk_category WHERE id = '{$param['id']}'");
                u("UPDATE mk_category SET rstatus = 'X',mdt=now(),mid='{$this->user}' WHERE id = '{$param['id']}'");
                $retrunarr = array('status' => TRUE, 'msg' => 'Deleted successfully :' . $param['cgid'], 'title' => 'Category', 'res' => array('id' => $param['cgid']));
                echo json_encode($retrunarr);
                die;
                break;
            default:
                break;
        }
    }

    function productCRUD($param, $formaction) {
        $pcode = isset($param['pcode']) ? substr(strtoupper($this->cleanString($param['pcode'])), 0, 30) : '';
        $pdesc = isset($param['pdesc']) ? substr(ucwords($this->cleanString($param['pdesc'])), 0, 200) : '';
        $cgid = isset($param['cgid']) ? $param['cgid']['cgid'] : '';
        $ptitle = isset($param['ptitle']) ? $param['ptitle'] : '';
        $pprice = isset($param['pprice']) ? $param['pprice'] : '';
        switch (strtolower($formaction)) {
            case 'create':
            case 'new':
                if ($pcode != "" && $pdesc != "" & $cgid != "") {
                    $res = so("SELECT pid FROM mk_product ORDER  BY pid DESC LIMIT 1");
                    $pid = '1000' + 1;
                    if ($res != '') {
                        $pid = $res['pid'] + 1;
                    }
                    $q = "INSERT INTO `mk_product` (`pid`,`pcode`,`cgid`,`ptitle`,`pprice`,`pdesc`, `rstatus`, `cid`, `cdt`, `mid`, `mdt`) VALUES ('{$pid}','{$pcode}', '{$cgid}','{$ptitle}','{$pprice}', '{$pdesc}', 'A', 'ADMIN', now(), NULL, NULL)";
                    $rep = i($q);
                    if ($rep['errorInfo'][0] == '00000') {
                        $retrunarr = array('status' => TRUE, 'msg' => 'Created successfully :' . $pid, 'title' => 'Product', 'res' => array('id' => $pid));
                    } else {
                        $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Product', 'res' => array('id' => ''));
                    }
                } else {
                    $retrunarr = array('status' => FALSE, 'msg' => 'Fill all information.', 'title' => 'Product', 'res' => array('id' => ''));
                }
                echo json_encode($retrunarr);
                die;
                break;
            case 'edit':
            case 'update':
                $rep = u("UPDATE mk_product SET pcode = '{$pcode}',pdesc = '{$pdesc}',cgid='{$cgid}',ptitle='{$ptitle}',pprice='{$pprice}', mid='{$this->user}' ,mdt=now() WHERE id = '{$param['id']}'");
                if ($rep['errorInfo'][0] == '00000') {
                    $retrunarr = array('status' => TRUE, 'msg' => 'Updated successfully :' . $param['pid'], 'title' => 'Product', 'res' => array('id' => $param['pid']));
                } else {
                    $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Product', 'res' => array('id' => ''));
                }
                echo json_encode($retrunarr);
                die;
                break;
            case 'delete':
                u("UPDATE mk_product SET rstatus = 'X',mdt=now(),mid='{$this->user}' WHERE id = '{$param['id']}'");
                $retrunarr = array('status' => TRUE, 'msg' => 'Deleted successfully :' . $param['pid'], 'title' => 'Product', 'res' => array('cid' => $param['pid']));
                echo json_encode($retrunarr);
                die;
                break;
            default:
                break;
        }
    }
    
    function addToCart($param, $formaction) {
        $pid = $param;
        $sql = "SELECT count(qty) AS qty FROM mk_cart WHERE uid = '{$this->user}' AND pid = '{$pid}' LIMIT 1";
        $res = so($sql);
        if ($res['qty'] == 0) {
            $q = "INSERT INTO mk_cart (uid, pid, qty, cid, cdt, mid, mdt) VALUES ('{$this->user}', '{$pid}', '1', '{$this->user}', now(), NULL, NULL)";
            $rep = i($q);
            if ($rep['errorInfo'][0] == '00000') {
                $retrunarr = array('status' => TRUE, 'msg' => 'Added successfully.', 'title' => 'Cart', 'res' => array('id' => $pid));
            } else {
                $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Cart', 'res' => array('id' => ''));
            }
        }else{
            $qty=$res['qty']+1;
            $rep = u("UPDATE mk_cart SET qty = '{$qty}', mid='{$this->user}' ,mdt=now() WHERE uid = '{$this->user}' AND pid = '{$pid}'");
            if ($rep['errorInfo'][0] == '00000') {
                $retrunarr = array('status' => TRUE, 'msg' => 'Added successfully.', 'title' => 'Cart', 'res' => array('id' => $pid));
            } else {
                $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Cart', 'res' => array('id' => ''));
            } 
        }
        echo json_encode($retrunarr);
        die;
    }
    
    function buyNow($param, $formaction) {
        $pid = $param;
        $res = so("SELECT id FROM mk_order ORDER BY id DESC LIMIT 1");
        $pd = so("SELECT pprice FROM mk_product WHERE pid = '{$pid}' LIMIT 1");
        $ym = date('ym');
        $ordid = '1000' + 1;
        $ordid = $ym . $ordid;
        if ($res != '') {
            $ordid = $res['id'] + 1;
            $ordid = '1000' + $ordid;
            $ordid = $ym . $ordid;
        }
        $q = "INSERT INTO mk_order (ordid,uid,pid,pamt,cid,cdt) VALUES ('{$ordid}','{$this->user}','{$pid}',{$pd['pprice']},'{$this->user}',now())";
        $rep = i($q);
        if ($rep['errorInfo'][0] == '00000') {
            $sql = "SELECT count(qty) AS qty FROM mk_cart WHERE uid = '{$this->user}' AND pid = '{$pid}' LIMIT 1";
            $res = so($sql);
            if ($res['qty']!= 0) {
                d("DELETE FROM mk_cart WHERE uid = '{$this->user}' AND pid = '{$pid}'");
            }
            $retrunarr = array('status' => TRUE, 'msg' => 'Order completed.', 'title' => 'Buy', 'res' => array('id' => $pid));
        } else {
            $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Buy', 'res' => array('id' => ''));
        }
        echo json_encode($retrunarr);
        die;
    }
    
    function removeFromCart($param, $formaction) {
        $pid = $param;
        d("DELETE FROM mk_cart WHERE uid = '{$this->user}' AND pid = '{$pid}'");
        $retrunarr = array('status' => TRUE, 'msg' => 'Removed successfully.', 'title' => 'Cart', 'res' => array('id' => $pid));
        echo json_encode($retrunarr);
        die;
    }
    
    function updateOrder($param, $formaction) {
        $orderid=$param['ordid'];
        switch (strtolower($formaction)) {
            case 'create':
            case 'new':
                $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Order', 'res' => array('id' => ''));
                echo json_encode($retrunarr);
                die;
                break;
            case 'edit':
            case 'update':
                $sts=$param['deliver']['key'];
                $rep = u("UPDATE mk_order SET deliver = '{$sts}',mid='{$this->user}' ,mdt=now() WHERE id = '{$param['id']}'");
                if ($rep['errorInfo'][0] == '00000') {
                    $retrunarr = array('status' => TRUE, 'msg' => 'Updated successfully :' . $orderid, 'title' => 'Order', 'res' => array('id' => $orderid));
                } else {
                    $retrunarr = array('status' => FALSE, 'msg' => 'Oops. something went wrong.', 'title' => 'Category', 'res' => array('id' => ''));
                }
                echo json_encode($retrunarr);
                die;
                break;
            case 'delete':
                u("UPDATE mk_order SET rstatus = 'X',mdt=now(),mid='{$this->user}' WHERE id = '{$param['id']}'");
                $retrunarr = array('status' => TRUE, 'msg' => 'Deleted successfully :' . $orderid, 'title' => 'Order', 'res' => array('id' => $orderid));
                echo json_encode($retrunarr);
                die;
                break;
            default:
                break;
        }
    }

}
