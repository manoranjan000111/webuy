<?php

try {
    $dbPDO = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD);
    $dbPDO->exec("set names utf8");
} catch (PDOException $e) {
    echo json_encode(array('status' => 1, 'error_message' => 'Unable to conect database...', 'data' => ''));
    die;
}

function so($sql, $qparams = array(), $unique = '') {
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_PARSE);
    global $dbPDO;
    try {
        $stmt = $dbPDO->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute($qparams);
        return $stmt->fetch(PDO::FETCH_ASSOC | $unique);
    } catch (PDOException $e) {
        trace($e);
    }
}

function sa($sql, $qparams = array(), $unique = '') {
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_PARSE);
    global $dbPDO;
    try {
        $stmt = $dbPDO->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute($qparams);
        return $stmt->fetchAll(PDO::FETCH_ASSOC | $unique);
    } catch (PDOException $e) {
        trace($e);
    }
}

function i($sql, $params = array(), $last_insert_id_column_name = 'id') {
    global $dbPDO;
    try {
        $stmt = $dbPDO->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $returnData['status'] = $stmt->execute($params);
        $returnData['lastInsertId'] = $dbPDO->lastInsertId($last_insert_id_column_name);  // Last Insert Id  
        $returnData['errorInfo'] = $stmt->errorInfo();
    } catch (PDOException $e) {
        trace($e);
    }
    return $returnData;
}

function u($sql, $params = array()) {
    global $dbPDO;
    try {
        $stmt = $dbPDO->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $returnData['status'] = $stmt->execute($params);
        $returnData['errorInfo'] = $stmt->errorInfo();
    } catch (PDOException $e) {
        trace($e);
    }
    return $returnData;
}

function d($sql, $params = array()) {
    global $dbPDO;
    try {
        $stmt = $dbPDO->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $returnData['status'] = $stmt->execute($params);
        $returnData['errorInfo'] = $stmt->errorInfo();
    } catch (PDOException $e) {
        err('<pre>' . $e->getMessage() . '</pre>');
    }
    return $returnData;
}

function tc($sql, $params = array()) {
    global $dbPDO;
    try {
        $dbPDO->query($sql);
        return '';
    } catch (PDOException $e) {
        return $e->getMessage();
    }
}
