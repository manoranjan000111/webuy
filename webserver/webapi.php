<?php

error_reporting(1);
error_reporting(E_ALL);
//if (isset($_SERVER['HTTP_ORIGIN'])) {
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Cache-Control: max-age=2592000");
//}
require_once 'config.php';
require_once "db.php";
require_once "constant.php";
require_once 'autoload.php';

$class = isset($_REQUEST["controller"]) ? $_REQUEST["controller"] : false;
$method = isset($_REQUEST["method"]) ? $_REQUEST["method"] : $_SERVER['REQUEST_METHOD'];

if ($class && class_exists($class)) {
    $objc = new $class();
    if ($method && method_exists($objc, $method)) {
        $obj = $objc->$method($p = false);
    } else {
        echo json_encode(array('status' => 'error', 'message' => 'controller ' . $class . ', method ' . $method . ' not exist.'));
    }
} else {
    echo json_encode(array('status' => 'error', 'message' => 'controller ' . $class . ' not exist.'));
}


